package com.example.agenda;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    final ArrayList<Contacto> contactos = new ArrayList<>();
    private EditText edtNombre;
    private EditText edtTelefono;
    private EditText edtTelefono2;
    private EditText edtDireccion;
    private EditText edtNotas;
    private CheckBox cbxFavorito;
    private Contacto saveContact;
    private int savedIndex = 1;
    private boolean isEdit = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNombre = (EditText) findViewById(R.id.edtNombre);
        edtTelefono = (EditText) findViewById(R.id.edtTel1);
        edtTelefono2 = (EditText) findViewById(R.id.edtTel2);
        edtDireccion = (EditText) findViewById(R.id.edtDomicilio);
        edtNotas = (EditText) findViewById(R.id.edtNota);
        cbxFavorito = (CheckBox) findViewById(R.id.chkFavorito);
        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);
        Button btncerrar = (Button) findViewById(R.id.btnCerrar);

        btncerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtNombre.getText().toString().matches("") || edtTelefono.getText().toString().matches("") ||
                        edtTelefono2.getText().toString().matches("") || edtDireccion.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this, "Ingrese todos los datos", Toast.LENGTH_SHORT).show();
                } else {
                    Contacto nContacto = new Contacto();
                    nContacto.setNombre(edtNombre.getText().toString());
                    nContacto.setDomicilio(edtDireccion.getText().toString());
                    nContacto.setNotas(edtNotas.getText().toString());
                    nContacto.setTelefono1(edtTelefono.getText().toString());
                    nContacto.setTelefono2(edtTelefono2.getText().toString());
                    nContacto.setFavorito(cbxFavorito.isChecked());
                    if(!isEdit)
                    {
                        nContacto.setID(savedIndex);
                        contactos.add(nContacto);
                        savedIndex++;
                    }
                    else{
                        nContacto.setID(saveContact.getID());
                        editContacto(nContacto);
                    }

                    Toast.makeText(MainActivity.this, R.string.mensaje, Toast.LENGTH_SHORT).show();
                    limpiar();
                }
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListActivity.class);
                Bundle bObject = new Bundle();
                bObject.putSerializable("contactos", contactos);
                i.putExtras(bObject);
                startActivityForResult(i, 0);
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
    }

    private void editContacto(Contacto contacto)
    {
        for(int x = 0; x < contactos.size(); x++)
        {
            if(contactos.get(x).getID() == contacto.getID())
            {
                this.contactos.set(x, contacto);
                break;
            }
        }
    }





    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(intent != null) {
            Bundle oBundle = intent.getExtras();
            int typeAction = oBundle.getInt(getString(R.string.action));
            this.contactos.clear();
            this.contactos.addAll((ArrayList<Contacto>) oBundle.getSerializable("list"));
            if(typeAction == 1)
            {
                saveContact = (Contacto) oBundle.getSerializable("contactos");
                edtNombre.setText(saveContact.getNombre());
                edtTelefono.setText(saveContact.getTelefono1());
                edtTelefono2.setText(saveContact.getTelefono2());
                edtDireccion.setText(saveContact.getDomicilio());
                edtNotas.setText(saveContact.getNotas());
                cbxFavorito.setChecked(saveContact.isFavorito());
                isEdit = true;
            }
        } else {
            limpiar();
        }
    }


    public void limpiar() {
        edtNombre.setText("");
        edtDireccion.setText("");
        edtTelefono.setText("");
        edtTelefono2.setText("");
        edtNotas.setText("");
        cbxFavorito.setChecked(false);
        saveContact = null;
        isEdit = false;
    }
}